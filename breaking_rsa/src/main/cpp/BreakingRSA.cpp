#include <iostream>
#include <stdlib.h>
#include <math.h>


///
// This is just a basic brute force approach. 
//
// @param cypher the target value 
// @param n the number to mod by
// @param value the array which the values will be stored in 
// @param spot the location in the array. 
///
void RSAStuff(unsigned long long int cypher, unsigned long long int n, 
    unsigned long long int *values, int* spot){

    unsigned long long int m;
    for(unsigned long long int i = 0; i < n; ++i){
      m = (((i * i) % n) * i) % n;
      if(m == cypher){
        values[spot[0]] = i;
        spot[0] = spot[0] + 1;
      }
    }
}

///
// The main function. All it does is take in input, sets up the memory 
// locations, and then copies them out.
//
// @param argc  the number of command line arguments
// @param argv  the actual command line arguments. 
///
int main(int argc, char *argv[]){
  if(argc != 3){
    std::cout << "./BreakingRSA <int> <int>\n";
  }

  // Converting the strings unsigned long long ints 
  char *pEnd;
  unsigned long long int cypher = strtoull(argv[1], &pEnd, 10);
  unsigned long long int n = strtoull(argv[2], &pEnd, 10);

  // Setting up the array for the index values. 
  // Setting up the array index value for the array on both the host and device
  unsigned long long int *values;
  int *h_spot;

  // Allocating space on this machine. 
  h_spot = (int*)malloc(sizeof(int));
  *h_spot = 0;

  // Allocating space on the device. 
  values = (unsigned long long int *) malloc(sizeof(unsigned long long int) * 3);
  values[0] = 0;
  values[1] = 0;
  values[2] = 0;
  //cudaMemcpy(d_spot, h_spot, sizeof(int), cudaMemcpyHostToDevice);
 
  RSAStuff(cypher, n, values, h_spot);

  // Printing 
  for(int cntr = 0; cntr < *h_spot; cntr ++){
    std::cout << values[cntr] << "^3 = " << cypher << " (mod " << n << ")\n";
  }

  if(*h_spot == 0){
    std::cout << "No cube roots for " << cypher << " (mod " << n << ")\n";
  }

  // Freeing memory. 
  free(h_spot);
}
