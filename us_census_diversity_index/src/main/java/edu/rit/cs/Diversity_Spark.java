package edu.rit.cs;
/**
 * Calculates the diversity index for all US Counties, outputs the results in order of state name then county name.
 * @author Richard Joerger
 */

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.*;

public class Diversity_Spark {

    /**
     * The usage string
     */
    public static final String DatasetFile = "dataset/cc-est2017-alldata.csv";

    /**
     * Calculates the diversity index given the spark context. It loads the data, preforms the calculation, and then
     * outputs that data to a file.
     * @param spark the spark context you want to use.
     */
    public static void calcIndex(SparkSession spark){

        // Loading the data.
        Dataset ds = spark.read()
                .option("header", "true")
                .option("delimiter", ",")
                .option("inferSchema", "true")
                .csv(DatasetFile);


        // Selecting the columns I need to find the total population across different age groups and years.
        Dataset ds2 = ds
                .select(ds.col("STATE"), ds.col("COUNTY"), ds.col("STNAME"),
                    ds.col("TOT_POP"), ds.col("AGEGRP"), ds.col("CTYNAME"))
                .groupBy("STATE", "COUNTY", "STNAME", "CTYNAME")
                .agg(functions.sum("TOT_POP"));

        // Going through the data and summing the male and female columns
        Dataset ds3 = ds.select(ds.col("STATE"), ds.col("COUNTY"), ds.col("YEAR"),
                ds.col("WA_MALE").plus(ds.col("WA_FEMALE")),
                ds.col("BA_MALE").plus(ds.col("BA_FEMALE")),
                ds.col("IA_MALE").plus(ds.col("IA_FEMALE")),
                ds.col("AA_MALE").plus(ds.col("AA_FEMALE")),
                ds.col("NA_MALE").plus(ds.col("NA_FEMALE")),
                ds.col("TOM_MALE").plus(ds.col("TOM_FEMALE")));

        // Summing through the rows for each state, this is effectively compressing the years.
        ds3 = ds3.groupBy("STATE", "COUNTY")
                .sum("(WA_MALE + WA_FEMALE)", "(BA_MALE + BA_FEMALE)", "(IA_MALE + IA_FEMALE)",
                        "(AA_MALE + AA_FEMALE)", "(NA_MALE + NA_FEMALE)", "(TOM_MALE + TOM_FEMALE)")
                .sort("STATE", "COUNTY");

        // Aliasing the two datasets so that they can be merged
        ds3 = ds3.as("a");
        ds2 = ds2.as("b");

        // Joining the two tables together.
        Dataset ds4 = ds3.as("a").join(ds2.as("b"), ds3.col("a.STATE").equalTo(ds2.col("b.STATE"))
                .and(ds3.col("a.COUNTY").equalTo(ds2.col("b.COUNTY"))));

        // Computing the inner part of the sum for the first group.
        Dataset ds5 = ds4.withColumn("RunningDiversity",
                ds4.col("sum(TOT_POP)").minus(ds4.col("sum((WA_MALE + WA_FEMALE))"))
                        .multiply(ds4.col(("sum((WA_MALE + WA_FEMALE))"))));

        // Computing the inner part of the sum for the second group, including the sum from the first group.
        ds5 = ds5.withColumn("RunningDiversity",
                ds4.col("sum(TOT_POP)").minus(ds5.col("sum((BA_MALE + BA_FEMALE))"))
                        .multiply(ds5.col("sum((BA_MALE + BA_FEMALE))"))
                        .plus(ds5.col("RunningDiversity")));

        ds5 = ds5.withColumn("RunningDiversity",
                ds5.col("sum(TOT_POP)").minus(ds5.col("sum((IA_MALE + IA_FEMALE))"))
                        .multiply(ds5.col("sum((IA_MALE + IA_FEMALE))"))
                        .plus(ds5.col("RunningDiversity")));

        ds5 = ds5.withColumn("RunningDiversity",
                ds5.col("sum(TOT_POP)").minus(ds5.col("sum((AA_MALE + AA_FEMALE))"))
                        .multiply(ds5.col("sum((AA_MALE + AA_FEMALE))"))
                        .plus(ds5.col("RunningDiversity")));

        ds5 = ds5.withColumn("RunningDiversity",
                ds5.col("sum(TOT_POP)").minus(ds5.col("sum((NA_MALE + NA_FEMALE))"))
                        .multiply(ds5.col("sum((NA_MALE + NA_FEMALE))"))
                        .plus(ds5.col("RunningDiversity")));

        ds5 = ds5.withColumn("RunningDiversity",
                ds5.col("sum(TOT_POP)").minus(ds5.col("sum((TOM_MALE + TOM_FEMALE))"))
                        .multiply(ds5.col("sum((TOM_MALE + TOM_FEMALE))"))
                        .plus(ds5.col("RunningDiversity")));

        // Computing diversity * 1/T^2.
        Dataset ds6 = ds5.withColumn("index",
                ds5.col("RunningDiversity")
                        .divide(
                                ds5.col("sum(TOT_POP)").multiply(ds5.col("sum(TOT_POP)"))));

        // Showing all the data
        ds6.show();

        // Selecting only the columns needed for output
        ds6 = ds6.select(ds6.col("STNAME"), ds6.col("CTYNAME"), ds6.col("index"))
                .sort("STNAME", "CTYNAME");

        // Saving to file.
        JavaRDD temp = ds6.javaRDD();
        temp.repartition(1).saveAsTextFile("Data");

    }

    /**
     * This is the main function. It sets up the spark context and then calls to the spark code which does all the
     * computation
     * @param args the command line args, ignored.
     */
    public static void main(String [] args) {
        // Create a SparkConf that loads defaults from system properties and the classpat
        SparkConf sparkConf = new SparkConf();
        sparkConf.set("spark.master", "local[4]");
        //Provides the Spark driver application a name for easy identification in the Spark or Yarn UI
        sparkConf.setAppName("Diversity");

        // Creating a session to Spark. The session allows the creation of the
        // various data abstractions such as RDDs, DataFrame, and more.
        SparkSession spark = SparkSession.builder().config(sparkConf).getOrCreate();

        calcIndex(spark);

        // Creating spark context which allows the communication with worker nodes
        JavaSparkContext jsc = new JavaSparkContext(spark.sparkContext());
    }

}
