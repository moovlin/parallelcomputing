package edu.rit.cs;

/**
 * This is the multithreaded version of the Lemoine Conjecture test
 *
 * @author Richard Joerger
 */
public class Lemoine_Smp {
    /**
     * This is the usage text. The error should tell you what is wrong but this string should cover everything else
     */
    private static String usage = "Lemoine_Seq <odd_int> <odd_int>";


    /**
     * This is the main method. It runs the test in sequential form when not compiled with omp4j.
     * @param args the command line arguments
     */
  public static void main(String[] args){
      if(!LemoineIO.checkSysArgs(args)){
      System.out.println(usage);
        System.exit(1);
      }

      int lowerBound = LemoineIO.getLowerBound(args);
      int upperBound = LemoineIO.getUpperBound(args);

      // Making the max value and iterator
      LemoineMaxValue maxValue = new LemoineMaxValue();
      Prime.Iterator primeIts = new Prime.Iterator();

      // Generating the primes
      int[] primes = new int[upperBound];
      int primeVal = 0;
      int cntr = 0;
      for(cntr = 0;primeVal < lowerBound; cntr ++){
        primeVal = primeIts.next();
        if(primeVal > lowerBound)
            break;
        primes[cntr] = primeVal;
      }
      int end = cntr;

      // omp parallel for public(maxValue) private(found)
      for(int boundCntr = lowerBound; boundCntr <= upperBound; boundCntr = boundCntr + 2){
          boolean found = false;
          for(int qCntr = end - 1; qCntr >=0 && !found; qCntr --){
            int p = boundCntr - 2 * primes[qCntr];
            if(Prime.isPrime(p)){
                // omp critical
                {
                    maxValue.updateMaxes(boundCntr, p, primes[qCntr]);
                    found = true;
                }
            }
          }
      }
      System.out.println(LemoineIO.buildOutputString(maxValue.getMaxN(), maxValue.getMaxP(), maxValue.getMaxQ() ));
  }
}
