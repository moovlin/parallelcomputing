package edu.rit.cs;

/**
 * Handles the logic for figuring out what the max value is
 *
 * @author Richard Joerger
 */
public class LemoineMaxValue {

    private int maxP;
    private int maxN;
    private int maxQ;

    /**
     * Constructs a LemoineMaxValue object with all the values set to 0
     */
    public LemoineMaxValue(){
        maxP = 0;
        maxQ = 0;
        maxN = 0;
    }

    /**
     * checks the param values against those which are saved. If checkP is greater, it updates all values. If checkP
     * equals maxP and checkN is greater than maxN, it saves all values, otherwise it does nothing
     * @param checkN the n value you want to check
     * @param checkP the p value you want to check against
     * @param checkQ the q value you want to check against
     */
    public void updateMaxes(int checkN, int checkP, int checkQ){
        if(checkP > maxP){
            maxP = checkP;
            maxN = checkN;
            maxQ = checkQ;
        } else if (checkP == maxP && checkN > maxN){
            maxN = checkN;
            maxQ = checkQ;
        }
    }

    /**
     * Gets the value of maxP;
     * @return the value as an int
     */
    public int getMaxP(){
        return maxP;
    }

    /**
     * Gets the value of MaxQ
     * @return the value as an int
     */
    public int getMaxQ() {
        return maxQ;
    }

    /**
     * Gets the value of MaxN
     * @return the value as an int
     */
    public int getMaxN(){
        return maxN;
    }
}
