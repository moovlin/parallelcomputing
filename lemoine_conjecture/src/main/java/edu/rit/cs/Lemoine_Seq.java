package edu.rit.cs;

/**
 * This is the sequential version of the Lemoine Conjecture test
 *
 * @author Richard Joerger
 */
public class Lemoine_Seq {


    /**
     * This is the usage text. The error should tell you what is wrong but this string should cover everything else
     */
    private static String usage = "Lemoine_Seq <odd_int> <odd_int>";

    /**
     * This is the main method. It runs the test in sequential form. It does not support compilation with omp4j.
     * @param args the command line arguments
     */
    public static void main(String[] args){
        if(!LemoineIO.checkSysArgs(args)){
          System.out.println(usage);
          System.exit(1);
        }

        int lowerBound = LemoineIO.getLowerBound(args);
        int upperBound = LemoineIO.getUpperBound(args);

        // Making the max value and iterator
        LemoineMaxValue maxValue = new LemoineMaxValue();

        // Generating the primes
        Prime.Iterator primeIts = new Prime.Iterator();
        int[] primes = new int[upperBound];
        int primeVal = 0;
        int cntr = 0;
        for(cntr = 0;primeVal < lowerBound; cntr ++){
            primeVal = primeIts.next();
            if(primeVal > lowerBound)
                break;
            primes[cntr] = primeVal;
        }
        int end = cntr;

        for(int boundCntr = lowerBound; boundCntr <= upperBound; boundCntr = boundCntr + 2){
            boolean found = false;
            for(int qCntr = end - 1; qCntr >=0 && !found; qCntr --){
                int p = boundCntr - 2 * primes[qCntr];
                if(Prime.isPrime(p)){
                    maxValue.updateMaxes(boundCntr, p, primes[qCntr]);
                    found = true;
                }
            }
        }

        System.out.println(LemoineIO.buildOutputString(maxValue.getMaxN(), maxValue.getMaxP(), maxValue.getMaxQ() ));
    }
}
