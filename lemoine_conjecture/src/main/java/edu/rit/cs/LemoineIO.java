package edu.rit.cs;

/**
 * I/O operations for the Lemoine conjecture program
 *
 * @author Richard Joerger
 */
public class LemoineIO {

    /**
     * building the string which gets output
     * @param n the odd integer
     * @param p the first prime
     * @param r the second prime
     * @return the string which represents the output
     */
    public static String buildOutputString(int n, int p, int r){
        return String.format("%d = %d + 2*%d", n, p, r);
    }

    /**
     * gets the lower bound value from the system args
     * @param args the argument list as a string
     * @return the lower bound as an int, -1 if there was a parse error but that shouldn't happen
     */
    public static int getLowerBound(String[] args){
        try {
            return Integer.parseInt(args[0]);
        } catch(NumberFormatException parserError){
            System.err.println("There was an issue parsing the lower bound");
        }
        return -1;
    }

    /**
     * Gets the upper bound value form the system args
     * @param args the argument list as a string
     * @return the upper bound as an int, -1 if there was a parse error but that shouldn't happen
     */
    public static int getUpperBound(String[] args){
        try {
            return Integer.parseInt(args[1]);
        } catch (NumberFormatException parserError){
            System.err.println("There was an issue parsing the upper bound");
        }
        return -1;
    }

    /**
     * checks that the arguments being passed in are valid
     * @param args the String argument list
     * @return true if they pass the test, false otherwise.
     */
    public static boolean checkSysArgs(String[] args){
        if(args.length != 2){
            System.err.println("Incorrect number of arguments");
            return false;
        }

        if(getLowerBound(args) == -1 || getUpperBound(args) == -1){
            return false;
        }

        if(getLowerBound(args) < 5){
            System.err.println("Your lower bound is less than 5");
            return false;
        }

        if(getLowerBound(args) > getUpperBound(args)){
            System.err.println("Your upper bound is less than your lower bound");
            return false;
        }

        if(getLowerBound(args) % 2 == 0){
            System.err.println("Your lower bound was even");
            return false;
        }

        if(getUpperBound(args) % 2 == 0){
            System.err.println("Your upper bound was even");
            return false;
        }

        return true;
    }
}
