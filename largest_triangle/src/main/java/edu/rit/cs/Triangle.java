/**
 * Represents a triangle for the purpose of this assignment.
 *
 * @author Richard Joerger
 */

package edu.rit.cs;

import java.lang.Math;
import java.math.BigDecimal;
import java.lang.StringBuilder;
import java.util.ArrayList;
import java.nio.ByteBuffer;

/**
 * It's a triangle, how exciting.
 */
public class Triangle {
    /**
     * The size of the triangle object in bytes.
     */
    public static final int TRIANGLE_BYTE_SIZE = 60;
    private Point first;
    private int firstInd;

    private Point second;
    private int secondInd;

    private Point third;
    private int thirdInd;

    private double area;


    /**
     * The constructor for a triangle. We'll always know all the points and their index when being constructed so
     * lets do that.
     * @param first the first point of the triangle
     * @param firstInd the index at which the first point was found
     * @param second the second point of the triangle
     * @param secondInd the index at which the second point was found
     * @param third the third point of the triangle
     * @param thirdInd the index at which the third point was found
     */
    public Triangle(Point first, int firstInd, Point second, int secondInd,
        Point third, int thirdInd){


        this.first = first;
        this.second = second;
        this.third = third;

        this.firstInd = firstInd;
        this.secondInd = secondInd;
        this.thirdInd = thirdInd;

        area = calcArea();
    }

    /**
     * Constructs a temp triangle with an area of -1.
     */
    public Triangle(){
        area = -1;
    }

    /**
     * Calculates the euclidan distance between two points
     *
     * @param one the first point you want the length of 
     * @param two the second you want the length of 
     * @return the distance between these two points. 
     */
    private double length(Point one, Point two){
        double len =  Math.pow(Math.pow((one.getX() - two.getX()), 2.0) + (Math.pow(one.getY() - two.getY(), 2.0)), 0.5);
        return len;
    }

    /**
     * Calculates the s value for this triangle
     *
     * @param a the first side length 
     * @param b the second side length 
     * @param c the third side length 
     */
    private double sVal(double a, double b, double c){
        return (a + b + c)/2.0;
    }

    /**
     * Calculates the area of a triangle 
     *
     * @return the area of this triangle 
     */
    private double calcArea(){

        double a = length(first, second);
        double b = length(second, third);
        double c = length(third, first);

        double s = sVal(a, b, c);

        double calcedArea =  Math.pow(s * (s - a) * (s - b) * (s - c), 0.5);

        return calcedArea;
    }

    /**
     * Returns the precalcualted area value. This just greatly reduces the number of floating point operations.
     * @return the area of this triangle.
     */
    public double area(){
        return area;
    }

    /**
     * Prints the info string for this triangle.
     * @return the string representation of the triangle
     */
    public String printInfo(){
        StringBuilder output = new StringBuilder();
        int index = getIndex1();
        double x = getPoint1().getX();
        double y = getPoint1().getY();

        output.append(String.format("%d %.5g %.5g%n", index, x, y));
        index = getIndex2();
        x = getPoint2().getX();
        y = getPoint2().getY();
        output.append(String.format("%d %.5g %.5g%n", index, x, y));

        index = getIndex3();
        x = getPoint3().getX();
        y = getPoint3().getY();
        output.append(String.format("%d %.5g %.5g%n", index, x, y));
        
        double area = area();
        output.append(String.format("%.5g%n", area));

        return output.toString();

    }

    /**
     * Gets the first point
     * @return point1
     */
    public Point getPoint1(){
        return first;
    }

    /**
     * Gets the second point
     * @return point2
     */
    public Point getPoint2(){
        return second;
    }

    /**
     * Gets the third point
     * @return point3
     */
    public Point getPoint3(){
        return third;
    }

    /**
     * Gets the first index
     * @return indexOne
     */
    public int getIndex1(){
        return firstInd;
    }

    /**
     * Gets the second index
     * @return index two
     */
    public int getIndex2(){
        return secondInd;
    }

    /**
     * Gets the third index
     * @return index three
     */
    public int getIndex3(){
        return thirdInd;
    }

    /**
     * Constructs a bytebuffer for this triangle
     * @return a refernce to a bytebuffer which has all the information of this triangle in it
     */
    public ByteBuffer getByteBuffer(){
        ByteBuffer sendTriBuf = ByteBuffer.allocate(60);
        sendTriBuf.putDouble(first.getX());
        sendTriBuf.putDouble(first.getY());
        sendTriBuf.putInt(firstInd);
        
        sendTriBuf.putDouble(second.getX());
        sendTriBuf.putDouble(second.getY());
        sendTriBuf.putInt(secondInd);

        sendTriBuf.putDouble(third.getX());
        sendTriBuf.putDouble(third.getY());
        sendTriBuf.putInt(thirdInd);

        return sendTriBuf;
    }

    /**
     * Gets the byte array representation of this triangle
     * @return a byte array which has the values of the triangle in it
     */
    public byte[] getByteRep(){
      return getByteBuffer().array();
    }

    /**
     * Static method which converts a byte array into an array of triangles.
     * @param triangleBytes the byte representation of a triangle
     * @return a Triangle array which has 0 to n triangles where n is the size of the group you're in.
     */
    public static Triangle[] bytesToTriangle(byte[] triangleBytes){
      ByteBuffer readBuffer = ByteBuffer.wrap(triangleBytes);
      ArrayList<Triangle> retList = new ArrayList<>();
      for(int cntr = 0; cntr < triangleBytes.length / 60; cntr ++){
          double x1 = readBuffer.getDouble();
          double y1 = readBuffer.getDouble();
          int index1 = readBuffer.getInt();

          double x2 = readBuffer.getDouble();
          double y2 = readBuffer.getDouble();
          int index2 = readBuffer.getInt();

          double x3 = readBuffer.getDouble();
          double y3 = readBuffer.getDouble();
          int index3 = readBuffer.getInt();

          Point p1 = new Point(x1, y1);
          Point p2 = new Point(x2, y2);
          Point p3 = new Point(x3, y3);

          retList.add(new Triangle(p1, index1, p2, index2, p3, index3));
      }

      return retList.toArray(new Triangle[triangleBytes.length % 60]);
    }

    /**
     * Tests if this triangle is less than, area and indicies, the triangle passed in
     * @param test the triangle you want to test if your area and what not is less than
     * @return true if it is less than you, false otherwise.
     */
    public boolean triangleLessThan(Triangle test){
      if(area() < test.area()){
        return true;
      } else if(area() > test.area()){
          return false;
      }

      if(firstInd > test.getIndex1()){
        return true;
      } else if (firstInd < test.getIndex1()){
          return false;
      }

      if (secondInd > test.getIndex2()){
        return true;
      } else if (secondInd < test.getIndex2()){
          return false;
      }

      if(thirdInd > test.getIndex3()){
        return true;
      }
      return false;
    }

}
