/**
 * Finds the largest triangle with the given points and generation
 *
 * @author Richard Joerger
 */

package edu.rit.cs;

import mpi.*;
import mpi.MPI;
import mpi.Datatype;
import mpi.MPIException;
import java.lang.Math;
import java.nio.IntBuffer;
import java.util.ArrayList;

/**
 * The largest triangle class, it handles all the logic for setup, inter process communication, and verification
 */
public class LargeTriangle_MPI
{

    /**
     * Checks that none of three points are equal.
     * @param p1 the first point
     * @param p2 the second point
     * @param p3 the third point
     * @return true if the points are different, false otherwise.
     */
    public static boolean checkDiffPoints(Point p1, Point p2, Point p3){
        if(p1.getX() == p2.getX() && p1.getY() == p2.getY()){
            return false;
        }

        if(p1.getX() == p3.getX() && p1.getY() == p3.getY()){
            return  false;
        }

        if(p2.getX() == p3.getX() && p2.getY() == p3.getY()){
            return false;
        }
        return  true;
    }

    /**
     * Checks that the two points are different
     * @param p1 the first point
     * @param p2 the second point
     * @return true if the points are difference, false otherwise
     */
    public static boolean checkTwoDiffPoints(Point p1, Point p2){
        if(p1.getX() == p2.getX() && p1.getY() == p2.getY()){
            return false;
        }
        return true;
    }

    /**
     * Represents what we expect the rank of the root node to be
     */
    private static final int ROOT_RANK = 0;
    /**
     * Main method
     * @param args the arguments
     * @throws MPIException this gets thrown if MPI breaks.
     */
    public static void main( String[] args ) throws MPIException {
        // Parsing command line args
        int numPoints = Integer.parseInt(args[0]);
        double side = Double.parseDouble(args[1]);
        long seed = Long.parseLong(args[2]);
        RandomPoints points1 = new RandomPoints(numPoints, side, seed);

        // Initing MPI and getting values that will be needed across the program
        MPI.Init(args);
        MPI.COMM_WORLD.setErrhandler(MPI.ERRORS_RETURN);
        int rank = MPI.COMM_WORLD.getRank();
        int size = MPI.COMM_WORLD.getSize(); 


        // Creating the buffer which the root node will eventually put the start and end points into.
        IntBuffer startPoints = IntBuffer.allocate(size * 2);

        if(rank == ROOT_RANK){
            // As the root node, we have to setup the ranges for all the nodes
            int splitSize = (int) Math.floor(numPoints/size * 1.0);
            int cntr = 0;
            for(;cntr < size - 1; cntr ++){
                startPoints.put(cntr * splitSize);
                startPoints.put(cntr * splitSize + splitSize);
            }
            startPoints.put(cntr * splitSize);
            startPoints.put(numPoints);
        }
       
        // Creating the array which will store our start and end values. Index 0 is the start, Index 1 is the end.
        int[] startEnd = new int[2];

        // Scattering our values out if we're, root and then all nodes are recieving their values into startEnd
        MPI.COMM_WORLD.scatter(startPoints.array(), 2, MPI.INT, startEnd, 2, MPI.INT, ROOT_RANK);

        // Generating all the points for the first level such that we can use the indexes we just got from the root
        Point[] firstPoints = new Point[numPoints];
        for(int cntr = 0; points1.hasNext(); cntr ++){
            firstPoints[cntr] = points1.next();
        }
        /*
         * Iterating through the entire array from this node's start and end points. Then, we use the two point
         * generators to "find" the other points which construct the largest triangle in our range.
         */
        int idx=1;
        Point p;
        Point q;
        Point z;
        Triangle maxTest = new Triangle();
        Triangle testTriangle = new Triangle();
        for(idx = startEnd[0]; idx < startEnd[1];) {
            p = firstPoints[idx];
            //RandomPoints points2 = new RandomPoints(numPoints, side, seed);
            int idx2 = 0;
            while (idx2 < numPoints){
                q = firstPoints[idx2];
                //RandomPoints points3 = new RandomPoints(numPoints, side, seed);
                if(!checkTwoDiffPoints(p, q)){
                    break;
                }
                int idx3 = 0;
                while(idx3 < numPoints){
                    z = firstPoints[idx3];
                    if(!checkDiffPoints(p, q, z)){
                        break;
                    }
                    testTriangle = new Triangle(p, idx + 1, q, idx2 + 1, z, idx3 + 1);
                    if(maxTest.area() < testTriangle.area()){
                        maxTest = testTriangle;
                    }
                    idx3 ++;
                }
                idx2 ++;
            }
            idx++;
        }
        // Setting up the byte array to which we will receive all of our information from each node.
        byte[] recvBytes = new byte[Triangle.TRIANGLE_BYTE_SIZE * size];

        // Array which will store our byte -> triangle triangles
        Triangle[] triangleSearch = null;

        // The bytes we're sending
        byte[] sendBytes = null;

        // If our max test triangle isn't null, convert it to it's byte form.
        if(maxTest != null){
          sendBytes = maxTest.getByteRep();
        } else {
          // This is bad and should never happen but we want to fail semigracefully.
          sendBytes = new byte[Triangle.TRIANGLE_BYTE_SIZE];
          System.out.format("test was null: start: %d, end: %d, rank: %d\n", startEnd[0], startEnd[1], rank);
        }

        // We're going to gather if we're the root node. This also handles sending our largest triangle to the root.
        MPI.COMM_WORLD.gather(sendBytes, Triangle.TRIANGLE_BYTE_SIZE, MPI.BYTE, recvBytes,
                Triangle.TRIANGLE_BYTE_SIZE, MPI.BYTE, ROOT_RANK);

        // If we're the root node, lets's check the list of max triangles. All this does is iterate across the list
        // and find the biggest one.
        if(rank == ROOT_RANK){
            triangleSearch = Triangle.bytesToTriangle(recvBytes);
            Triangle max = null;
            try {
              max = triangleSearch[0];
            } catch (java.lang.ArrayIndexOutOfBoundsException e){
              System.out.println(e.toString());
            }
            for(int cntr = 1; cntr < triangleSearch.length; cntr ++){
              Triangle testMax = triangleSearch[cntr];
              if(max.triangleLessThan(testMax)){
                  max = testMax;
              }
            }

            // Printing our max value.
            System.out.println(max.printInfo());
        }

        // Closing out MPI correctly.
        MPI.Finalize();
    }
}
