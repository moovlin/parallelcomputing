package edu.rit.cs;


public class LargeTriangle
{

    public static void main( String[] args ) {
        int numPoints = Integer.parseInt(args[0]);
        double side = Double.parseDouble(args[1]);
        long seed = Long.parseLong(args[2]);
        RandomPoints points1 = new RandomPoints(numPoints, side, seed);
        int idx=1;
        Point p;
        double maxArea = -1.0;
        Triangle test = null;
        while(points1.hasNext()) {
            p = points1.next();
            RandomPoints points2 = new RandomPoints(numPoints, side, seed);
            int idx2 = 1;
            while (points2.hasNext()){
                Point q = points2.next();
                RandomPoints points3 = new RandomPoints(numPoints, side, seed);
                int idx3 = 1;
                while(points3.hasNext()){
                    Point z = points3.next();
                    Triangle testTriangle = new Triangle(p, idx, q, idx2, z, idx3);
                    if(maxArea < testTriangle.area()){
                        maxArea = testTriangle.area();
                        test = testTriangle;
                    } else if (maxArea == testTriangle.area()) {
                      if(test.getIndex1() >= testTriangle.getIndex1()){
                        if(test.getIndex2() >= testTriangle.getIndex2()){
                          if(test.getIndex3() < testTriangle.getIndex3()){
                            test = testTriangle;
                          }
                        } else {
                            test = testTriangle;
                        }
                      } else {
                        test = testTriangle;
                      }
                    }
                    idx3 ++;
                }
                idx2 ++;
            }
            idx++;
        }
        System.out.println(test.printInfo());
    }
}
